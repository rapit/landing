# Beam

A Craft CMS landing page for Beam

----

## Requirements
This project assumes you have PHP 7+, MySQL or PostgreSQL and [Composer](https://getcomposer.org/) installed locally. Please see Craft's [requirements]( https://docs.craftcms.com/v3/requirements.html) and [installation guide](https://docs.craftcms.com/v3/installation.html) for more details.

----

## Installing
1. Clone this repo to your local machine
2. Save a copy of `.env.example` as `.env` in the root folder
3. Download the latest database from staging/production and import to a local database on your machine
4. Add your local database connection details and a randomly generated SECURITY_KEY to your newly created `.env` file
5. From the root folder, run `composer update`

----

## Project Structure

#### templates/
Your front-end templates go in here.

#### vendor/
This is where all of your Composer dependencies go, including Craft itself, and any plugins you’ve installed via Composer. This folder should not be committed.

#### web/
This directory represents your server’s webroot. (And contains your production-ready CSS/JS/assets).

#### config/
Holds all of your Craft and plugin configuration files, as well as your license.key file.

#### storage/
This is where Craft stores a bunch of files that get dynamically generated at runtime.

See the [official docs](https://docs.craftcms.com/v3/directory-structure.html) for more information about Craft's structure
